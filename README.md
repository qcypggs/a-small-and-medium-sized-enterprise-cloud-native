# 一个中小企业的云原生之路

#### 介绍
构建中小企业的云原生之路，包括DevOps全部流程，cicd，监控，自动化，k8s,云原生，安全等等
目的是解放运维的双手，一起构建开源的世界。
规范：
域名规范：假如顶级域名为 cypggs.com 

业务层规范：
业务按产品线划分，产品线对应k8s的namespace 如有支付产品线 则为 pay
产品线分项目名称，项目名称统一为产品线+项目名称，如有支付网关 则为 pay-gateway
项目部署可以用deployment ,如果区分环境 deployment名称为  pay-gateway-dev ; svc同项目名称；ing为 pay-gateway-dev.cypggs.com
项目端口全部统一为 8080，包括前端项目也是

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
