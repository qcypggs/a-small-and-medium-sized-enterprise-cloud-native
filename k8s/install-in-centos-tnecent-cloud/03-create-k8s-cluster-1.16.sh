#/bin/bash
#安装1.16
#本脚本请拷贝到目标机器上执行,copy : scp 03-create-k8s-cluster-1.16.sh root@106.53.132.220:~
wget -c https://sealyun.oss-cn-beijing.aliyuncs.com/latest/sealos &&     chmod +x sealos && mv sealos /usr/bin
wget -c https://sealyun.oss-cn-beijing.aliyuncs.com/37374d999dbadb788ef0461844a70151-1.16.0/kube1.16.0.tar.gz
LOCALIP=`python -c "import socket;print([(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1])"`
sealos init --passwd HHXX@ttxs --master $LOCALIP --pkg-url ./kube1.16.0.tar.gz --version v1.16.0

#安装kuboard
kubectl apply -f https://kuboard.cn/install-script/kuboard.yaml
#kubectl apply -f https://addons.kuboard.cn/metrics-server/0.3.7/metrics-server.yaml
# 如果您参考 www.kuboard.cn 提供的文档安装 Kuberenetes，可在第一个 Master 节点上执行此命令
sleep 10
#让master也当node调度,生产环境不可使用这样的方式
kubectl taint node `kubectl get nodes|grep -v NAME|awk '{print $1}'` node-role.kubernetes.io/master-
#增加快捷方式，个人喜好
cat >>  ~/.bashrc << EOF
alias kg='kubectl get'
alias kc='kubectl apply -f'
alias ke='kubectl exec -it'
alias kd='kubectl describe'
alias kdp='kubectl describe pods'
alias kl='kubectl logs -f'
alias ktmp='kubectl run -i --tty --image busybox dns-test --restart=Never --rm /bin/sh'
EOF
source ~/.bashrc
#查看当前pods
kubectl get pods -A
echo "稍等几分钟访问："
echo http://`curl -s ip.cip.cc`:32567
echo "使用下面的k8s管理员token登陆kuboard管理平台（类似Dashboard，比它强大多了）"
echo $(kubectl -n kube-system get secret $(kubectl -n kube-system get secret | grep kuboard-user | awk '{print $1}') -o go-template='{{.data.token}}' | base64 -d)
