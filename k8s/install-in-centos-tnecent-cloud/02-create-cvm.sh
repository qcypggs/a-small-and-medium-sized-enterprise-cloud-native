#/bin/bash
#默认密码为HHXX@ttxs 其他参数需要调整请参考https://cloud.tencent.com/document/product/440/34012
if [ $# != 2 ] ; then
echo "USAGE: $0 实例规格 付费模式"
echo " e.g.: $0 S2.MEDIUM4 SPOTPAID"
echo "常用规格 S2.SMALL1（标准型S2，1核1GB）;S2.MEDIUM4（标准型S2，2核4GB）;S2.LARGE8（标准型S2，4核8GB）;S2.4XLARGE32（标准型S2，16核32GB） "
echo "付费模式 PREPAID：预付费，即包年包月；POSTPAID_BY_HOUR：按小时后付费；SPOTPAID：竞价付费；CDHPAID：独享子机（基于专用宿主机创建，宿主机部分的资源不收费）"
exit 1;
fi
tccli cvm RunInstances --InstanceChargeType $2 --InstanceChargePrepaid '{"Period":1,"RenewFlag":"DISABLE_NOTIFY_AND_MANUAL_RENEW"}' --Placement '{"Zone":"ap-guangzhou-2"}' --InstanceType $1 --ImageId img-9qabwvbn --SystemDisk '{"DiskType":"CLOUD_SSD", "DiskSize":50}' --InternetAccessible '{"InternetChargeType":"TRAFFIC_POSTPAID_BY_HOUR","InternetMaxBandwidthOut":100,"PublicIpAssigned":true}' --InstanceCount 1 --InstanceName GZ-CVM-$1 --LoginSettings '{"Password":"HHXX@ttxs"}' --SecurityGroupIds '["sg-8wri1w7d"]' --HostName "GZ-CVM-$1"
sleep 15
tccli cvm DescribeInstances
tccli cvm DescribeInstances|grep PublicIpAddresses -A1|tail -n 1|cut -d\" -f2
