#!/bin/bash
#centos
yum install -y nfs-utils
#配置nfs
cat >> /etc/exports << EOF
/data/nfs-client *(insecure,rw,sync,no_root_squash)
EOF
#启动
mkdir -p /data/nfs-client
chmod -R 777 /data/nfs-client
systemctl enable rpcbind
systemctl enable nfs-server

systemctl start rpcbind
systemctl start nfs-server
exportfs -r
#验证
exportfs -r
exportfs
showmount -e localhost
#mount -vvv -t nfs localhost:/data/nfs-client /mnt
