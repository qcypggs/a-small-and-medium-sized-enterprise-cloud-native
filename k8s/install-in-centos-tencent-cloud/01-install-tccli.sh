#/bin/bash
#申请资源

#安装腾讯云管理工具tccli
#readme https://cloud.tencent.com/document/product/440/34012
yum -y install epel-release -y
yum install python-pip -y
pip install --upgrade pip
pip install tccli
tccli help && [ $? == 0 ] && echo "tccli install success"

#查看密钥https://console.cloud.tencent.com/cam/capi
#输入自己腾讯云账号的密钥其他默认即可，默认资源是广州
tccli configure
